package qlitzler.com.healsy;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;

import java.lang.ref.WeakReference;

import qlitzler.com.healsy.device.FragmentListDevice;

/**
 * Created by qlitzler on 01/05/16.
 */
public class BroadcastReceiverDiscovery extends BroadcastReceiver {

	private WeakReference<ActivityHome> reference;

	public BroadcastReceiverDiscovery(ActivityHome activity) {
		reference = new WeakReference<>(activity);
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		ActivityHome activity = reference.get();
		FragmentListDevice fragment = activity.findFragmentListDevice();
		String action = intent.getAction();

		switch (action) {
			case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
				fragment.discoveryStarted();
				break;
			case BluetoothDevice.ACTION_FOUND:
				discoveryFound(activity, fragment, intent);
				break;
			case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
				fragment.discoveryFinished();
				break;

		}
	}

	private void discoveryFound(ActivityHome activity, FragmentListDevice fragment, Intent intent) {
		BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		if (device.getName() != null && device.getAddress() != null) {
			fragment.addDevice(device);
			Snackbar
				.make(
					activity.getRoot(),
					String.format(
						activity.getString(R.string.discovery_found),
						device.getName()
					),
					Snackbar.LENGTH_SHORT
				)
				.show();
		}
	}
}
