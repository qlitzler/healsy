package qlitzler.com.healsy.messenger;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import java.util.ArrayList;

import qlitzler.com.healsy.R;
import qlitzler.com.lasagna.interfaces.IVAdapterCallback;
import qlitzler.com.lasagna.interfaces.IVLifecycle;
import qlitzler.com.lasagna.list.IVAdapterList;
import qlitzler.com.lasagna.list.IVFragmentList;

/**
 * Created by qlitzler on 08/05/16.
 */
public class FragmentListMessenger extends IVFragmentList<Messenger> {

	public static final String TAG = FragmentListMessenger.class.getName();

	@Override
	public String getIVTag() {
		return TAG;
	}

	@Override
	protected int getIdLayout() {
		return R.layout.fragment_list_messenger;
	}

	@Override
	protected int getIdList() {
		return R.id.list_messenger;
	}

	@Override
	public void adapterCallback(int position) {

	}

	@Override
	public void instantiate(View view) {
		super.instantiate(view);
		adapterList.setList(new ArrayList<>());
	}

	@Override
	public void update(Context context) {
		super.update(context);
	}

	@Override
	protected IVAdapterList<Messenger> getAdapterList(IVLifecycle lifecycle, LayoutInflater inflater, IVAdapterCallback callback) {
		return new AdapterListMessenger(lifecycle, inflater, callback);
	}

	public void addMessage(int type, String message) {
		adapterList.addObject(new Messenger(message, type));
		adapterList.notifyItemInserted(adapterList.getItemCount() - 1);
		recyclerView.smoothScrollToPosition(adapterList.getItemCount() - 1);
	}
}
