package qlitzler.com.healsy.messenger;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.jakewharton.rxbinding.view.RxView;
import com.rengwuxian.materialedittext.MaterialEditText;

import qlitzler.com.healsy.BackgroundServiceIO;
import qlitzler.com.healsy.BroadcastReceiverIO;
import qlitzler.com.healsy.R;
import qlitzler.com.healsy.rfcomm.Protocol;
import qlitzler.com.lasagna.activity.IVActivity;

/**
 * Created by qlitzler on 08/05/16.
 */
public class ActivityMessenger extends IVActivity {

	public static final String TYPE = "type";
	public static final String CLIENT = "client";
	public static final String NAME = "name";

	private Toolbar toolbar;
	private MaterialEditText editText;
	private ImageView send;
	private IntentFilter filterIO;
	private BroadcastReceiverIO receiverIO;

	@Override
	protected int getIdLayout() {
		return R.layout.activity_messenger;
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(receiverIO, filterIO);
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(receiverIO);
	}

	@Override
	public void enumerate(View view) {
		toolbar = (Toolbar) view.findViewById(R.id.toolbar);
		editText = (MaterialEditText) view.findViewById(R.id.editable_text);
		send = (ImageView) view.findViewById(R.id.send_message);
	}

	@Override
	public void instantiate(View view) {
		setSupportActionBar(toolbar);
		filterIO = BroadcastReceiverIO.filterMessenger();
		receiverIO = new BroadcastReceiverIO(this);
	}

	@Override
	public void update(Context context) {
		subscribe(RxView.clicks(send).filter(v -> !editText.getText().toString().isEmpty()).subscribe(this::send));
		setTitle(getIntent().getStringExtra(NAME));
	}

	private void send(Void aVoid) {
		Intent service = new Intent(this, BackgroundServiceIO.class);
		String message = editText.getText().toString();
		findFragmentListMessenger().addMessage(AdapterListMessenger.SENDER, message);
		service.setData(Uri.parse(getIntent().getStringExtra(TYPE)));
		service.putExtra(BackgroundServiceIO.MESSAGE, Protocol.MESSAGE + message);
		service.putExtra(BackgroundServiceIO.DEVICE_NAME, getIntent().getStringExtra(CLIENT));
		startService(service);
		editText.setText("");
	}

	public void displayError(String message) {
		Snackbar.make(root, message, Snackbar.LENGTH_LONG).show();
	}

	public FragmentListMessenger findFragmentListMessenger() {
		return (FragmentListMessenger) fm.findFragmentByTag(FragmentListMessenger.TAG);
	}
}
