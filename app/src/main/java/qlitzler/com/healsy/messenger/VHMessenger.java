package qlitzler.com.healsy.messenger;

import android.view.View;
import android.widget.TextView;

import qlitzler.com.healsy.R;
import qlitzler.com.lasagna.interfaces.IVAdapterCallback;
import qlitzler.com.lasagna.interfaces.IVLifecycle;
import qlitzler.com.lasagna.list.IVVHList;

/**
 * Created by qlitzler on 08/05/16.
 */
public class VHMessenger extends IVVHList<Messenger> {

	private TextView message;

	public VHMessenger(IVLifecycle lifecycle, View view, int typeView, IVAdapterCallback callback) {
		super(lifecycle, view, typeView, callback);
	}

	@Override
	public void enumerate(View view, int typeView) {
		message = (TextView) view.findViewById(R.id.message);
	}

	@Override
	public void update(Messenger messenger, int typeView) {
		message.setText(messenger.message);
	}
}
