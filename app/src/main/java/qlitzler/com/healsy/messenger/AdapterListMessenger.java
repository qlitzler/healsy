package qlitzler.com.healsy.messenger;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import qlitzler.com.healsy.R;
import qlitzler.com.lasagna.interfaces.IVAdapterCallback;
import qlitzler.com.lasagna.interfaces.IVLifecycle;
import qlitzler.com.lasagna.list.IVAdapterList;
import qlitzler.com.lasagna.list.IVVHList;

/**
 * Created by qlitzler on 08/05/16.
 */
public class AdapterListMessenger extends IVAdapterList<Messenger> {

	public static final int SENDER = 0;
	public static final int RECEIVER = 1;

	public AdapterListMessenger(IVLifecycle lifecycle, LayoutInflater inflater,
								IVAdapterCallback callback
	) {
		super(lifecycle, inflater, callback);
	}

	@Override
	protected View inflateView(LayoutInflater inflater, ViewGroup parent, int typeView) {
		final int id;

		switch (typeView) {
			case SENDER:
				id = R.layout.single_messenger_sender;
				break;
			case RECEIVER:
				id = R.layout.single_messenger_receiver;
				break;
			default:
				return null;
		}
		return inflater.inflate(id, parent, false);
	}

	@Override
	protected IVVHList<Messenger> getViewHolder(IVLifecycle lifecycle, View view, int typeView,
												IVAdapterCallback callback
	) {
		return new VHMessenger(lifecycle, view, typeView, callback);
	}

	@Override
	public int getItemViewType(int position) {
		return getObject(position).type;
	}
}
