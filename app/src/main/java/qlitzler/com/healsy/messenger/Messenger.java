package qlitzler.com.healsy.messenger;

import com.google.gson.annotations.SerializedName;

/**
 * Created by qlitzler on 08/05/16.
 */
public class Messenger {

	private static final String TYPE = "type";
	private static final String MESSAGE = "message";

	@SerializedName(TYPE)
	public final int type;

	@SerializedName(MESSAGE)
	public final String message;

	public Messenger(String message, int type) {
		this.type = type;
		this.message = message;
	}
}
