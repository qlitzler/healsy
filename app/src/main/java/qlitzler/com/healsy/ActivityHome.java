package qlitzler.com.healsy;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import qlitzler.com.healsy.device.FragmentListDevice;
import qlitzler.com.lasagna.IVApplication;
import qlitzler.com.lasagna.activity.IVActivity;

public class ActivityHome extends IVActivity {

	private static final int REQUEST_ENABLE_BLUETOOTH = 10;
	private static final int REQUEST_PERMISSION_LOCATION = 11;

	private Toolbar toolbar;
	private IntentFilter filterDiscovery, filterIO;
	private BroadcastReceiver receiverDiscovery, receiverIO;

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(receiverDiscovery, filterDiscovery);
		registerReceiver(receiverIO, filterIO);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (receiverDiscovery != null) {
			unregisterReceiver(receiverDiscovery);
		}
		if (receiverIO != null) {
			unregisterReceiver(receiverIO);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
			if (resultCode == RESULT_OK) {
				discover(0);
				Snackbar.make(root, getString(R.string.bluetooth_ok), Snackbar.LENGTH_SHORT).show();
			} else {
				Snackbar.make(root, getString(R.string.bluetooth_ko), Snackbar.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults
	) {
		if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				discover(0);
				Snackbar.make(root, getString(R.string.location_ok), Snackbar.LENGTH_SHORT).show();
			} else {
				Snackbar.make(root, getString(R.string.location_ko), Snackbar.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void enumerate(View view) {
		toolbar = (Toolbar) view.findViewById(R.id.toolbar);
	}

	@Override
	public void instantiate(View view) {
		setSupportActionBar(toolbar);
		filterDiscovery = filterDiscovery();
		filterIO = BroadcastReceiverIO.filterHome();
		receiverDiscovery = new BroadcastReceiverDiscovery(this);
		receiverIO = new BroadcastReceiverIO(this);
	}

	@Override
	public void update(Context context) {
		if (requestLocation() && requestBluetooth()) {
			discover(0);
		}
	}

	private void discover(final int duration) {
		Intent service = new Intent(this, BackgroundServiceIO.class);
		service.setData(Uri.parse(BackgroundServiceIO.SERVER_CREATE));
		startService(service);
		if (IVApplication.bluetoothAdapter.getScanMode()
			!= BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			intent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, duration);
			startActivity(intent);
		}
		discoverDevices();
	}

	public FragmentListDevice findFragmentListDevice() {
		return (FragmentListDevice) fm.findFragmentByTag(FragmentListDevice.TAG);
	}

	private static IntentFilter filterDiscovery() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(BluetoothDevice.ACTION_FOUND);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		return filter;
	}

	private boolean requestBluetooth() {
		if (AppHealsy.bluetoothAdapter == null) {
			Snackbar.make(root, getString(R.string.bluetooth_no), Snackbar.LENGTH_SHORT).show();
			return false;
		} else if (!AppHealsy.bluetoothAdapter.isEnabled()) {
			Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(intent, REQUEST_ENABLE_BLUETOOTH);
			return false;
		}
		return true;
	}

	private boolean requestLocation() {
		if (!AppHealsy.hasPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
			ActivityCompat.requestPermissions(
				this,
				new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
				REQUEST_PERMISSION_LOCATION
			);
			return false;
		}
		return true;
	}

	public void discoverDevices() {
		if (!AppHealsy.bluetoothAdapter.startDiscovery() && root != null) {
			Snackbar.make(root, getString(R.string.discovery_ko), Snackbar.LENGTH_SHORT).show();
			FragmentListDevice fragment = findFragmentListDevice();
			fragment.discoveryFinished();
		}
	}

	public void displayError(String message) {
		Snackbar.make(root, message, Snackbar.LENGTH_LONG).show();
	}

	public ViewGroup getRoot() {
		return root;
	}

	@Override
	protected int getIdLayout() {
		return R.layout.activity_home;
	}
}
