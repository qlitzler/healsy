package qlitzler.com.healsy.rfcomm;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.util.UUID;

import qlitzler.com.healsy.AppHealsy;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by qlitzler on 01/05/16.
 */
public class Client extends Rfcomm {

	public Client(BluetoothDevice device, UUID uuid) {
		rfcomm = Client.create(device, uuid).flatMap(Client::scheduler);
	}

	public static Observable<BluetoothSocket> scheduler(BluetoothSocket client) {
		return Client
			.connect(client)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread());
	}

	public static Observable<BluetoothSocket> create(BluetoothDevice device, UUID uuid) {
		return Observable.create(subscriber -> {
			try {
				subscriber.onNext(device.createRfcommSocketToServiceRecord(uuid));
				subscriber.onCompleted();
			} catch (IOException e) {
				subscriber.onError(e);
			}
		});
	}

	private static Observable<BluetoothSocket> connect(BluetoothSocket client) {
		return Observable.create(subscriber -> {
			try {
				AppHealsy.bluetoothAdapter.cancelDiscovery();
				client.connect();
				subscriber.onNext(client);
				subscriber.onCompleted();
			} catch (IOException e) {
				subscriber.onError(e);
			}
		});
	}
}
