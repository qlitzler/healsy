package qlitzler.com.healsy.rfcomm;

/**
 * Created by qlitzler on 05/05/16.
 */
public class Triplet<F, S, T> {

	public final F first;
	public final S second;
	public final T third;

	public Triplet(F first, S second, T third) {
		this.first = first;
		this.second = second;
		this.third = third;
	}
}
