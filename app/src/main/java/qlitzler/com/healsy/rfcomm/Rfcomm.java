package qlitzler.com.healsy.rfcomm;

import android.bluetooth.BluetoothSocket;
import android.util.Pair;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by qlitzler on 01/05/16.
 */
public class Rfcomm {

	protected Observable<BluetoothSocket> rfcomm;
	protected BluetoothSocket socket;
	protected InputStream in;
	protected OutputStream out;

	public Subscription start(Action1<Triplet<BluetoothSocket, InputStream, OutputStream>> action,
							  Action1<Throwable> error) {
		return rfcomm
			.flatMap(Rfcomm::streams)
			.doOnNext(triplet -> {
				socket = triplet.first;
				in = triplet.second;
				out = triplet.third;
			})
			.subscribe(action, error);
	}

	public boolean isConnected() {
		return socket != null && socket.isConnected();
	}

	public Observable<Pair<Integer, String>> read() {
		return Rfcomm.readLoop(in);
	}

	public void write(String message, Action1<Throwable> throwable) {
		Rfcomm.write(out, message).subscribe(v -> {}, throwable);
	}

	public void closeIO(Action1<Throwable> throwable) {
		if (in != null) {
			Rfcomm.close(socket).subscribe(v -> {}, throwable);
		}
		if (out != null) {
			Rfcomm.close(socket).subscribe(v -> {}, throwable);
		}
	}

	public void shutdown(Action1<Throwable> throwable) {
		Rfcomm.close(socket).subscribe(v -> {}, throwable);
	}

	public static Observable<Void> close(Closeable socket) {
		return Observable.create(subscriber -> {
			try {
				socket.close();
				subscriber.onNext(null);
				subscriber.onCompleted();
			} catch (IOException e) {
				subscriber.onError(e);
			}
		});
	}

	public static Observable<Triplet<BluetoothSocket, InputStream, OutputStream>> streams(BluetoothSocket socket) {
		return Observable.create(subscriber -> {
			try {
				subscriber.onNext(new Triplet<>(socket, socket.getInputStream(), socket.getOutputStream()));
				subscriber.onCompleted();
			} catch (IOException e) {
				subscriber.onError(e);
			}
		});
	}

	public static Observable<Pair<Integer, String>> readLoop(InputStream inputStream) {
		return Rfcomm
			.read(inputStream)
			.takeWhile(pair -> pair.first != -1)
			.repeat()
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread());
	}

	public static Observable<String> write(OutputStream outputStream, String message) {
		return Observable.create(subscriber -> {
			try {
				outputStream.write(message.getBytes());
				subscriber.onNext(message);
				subscriber.onCompleted();
			} catch (IOException e) {
				subscriber.onError(e);
			}
		});
	}

	private static Observable<Pair<Integer, String>> read(InputStream inputStream) {
		return Observable.create(subscriber -> {
			byte[] buffer = new byte[1024];
			int ret;

			try {
				ret = inputStream.read(buffer);
				subscriber.onNext(new Pair<>(ret, new String(buffer, 0, ret, "UTF-8")));
				subscriber.onCompleted();
			} catch (IOException e) {
				subscriber.onError(e);
			}
		});
	}

}
