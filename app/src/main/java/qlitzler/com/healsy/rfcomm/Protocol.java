package qlitzler.com.healsy.rfcomm;

/**
 * Created by qlitzler on 08/05/16.
 */
public class Protocol {

	public static final String CLIENT_CREATE = "client create: ";
	public static final String CLIENT_CLOSE = "client close: ";
	public static final String MESSAGE = "message/";
	public static final String ERROR = "error";

	public static String newClient(String name) {
		return CLIENT_CREATE + name;
	}

	public static String closeClient(String name) {
		return CLIENT_CLOSE + name;
	}
}
