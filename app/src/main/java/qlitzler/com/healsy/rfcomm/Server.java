package qlitzler.com.healsy.rfcomm;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.util.UUID;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by qlitzler on 01/05/16.
 */
public class Server extends Rfcomm {

	public Server(BluetoothAdapter adapter, String name, UUID uuid) {
		rfcomm = Server.create(adapter, name, uuid).flatMap(Server::scheduler);
	}

	private static Observable<BluetoothSocket> scheduler(BluetoothServerSocket server) {
		return Server
			.accept(server)
			.repeat()
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread());
	}

	private static Observable<BluetoothServerSocket> create(BluetoothAdapter adapter, String name, UUID uuid) {
		return Observable.create(subscriber -> {
			try {
				subscriber.onNext(adapter.listenUsingRfcommWithServiceRecord(name, uuid));
				subscriber.onCompleted();
			} catch (IOException e) {
				subscriber.onError(e);
			}
		});
	}

	private static Observable<BluetoothSocket> accept(BluetoothServerSocket server) {
		return Observable.create(subscriber -> {
			try {
				subscriber.onNext(server.accept());
				subscriber.onCompleted();
			} catch (IOException e) {
				subscriber.onError(e);
			}
		});
	}
}
