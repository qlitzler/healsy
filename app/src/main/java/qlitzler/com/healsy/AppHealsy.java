package qlitzler.com.healsy;

import android.content.Context;

import java.util.HashMap;
import java.util.UUID;

import qlitzler.com.healsy.rfcomm.Client;
import qlitzler.com.healsy.rfcomm.Server;
import qlitzler.com.lasagna.IVApplication;

/**
 * Created by qlitzler on 01/05/16.
 */
public class AppHealsy extends IVApplication {

	public static final String LOG_SERVER = "HServer";
	public static final String LOG_CLIENT = "HClient";
	public static final UUID HEALSY_UUID = UUID.fromString("7bba2ce5-2238-4974-add0-eadfdbf5fce7");

	private static AppHealsy appHealsy;
	private HashMap<String, Client> clients;

	private Server server;

	@Override
	protected void instantiate(Context context) {
		appHealsy = (AppHealsy) instance;
		server = new Server(AppHealsy.bluetoothAdapter, "HealsyServer", AppHealsy.HEALSY_UUID);
		clients = new HashMap<>();
	}

	public static Server getServer() {
		return appHealsy.server;
	}

	public static void addClient(String key, Client client) {
		appHealsy.clients.put(key, client);
	}

	public static Client getClient(String key) {
		return appHealsy.clients.get(key);
	}

	public static void removeClient(String key) {
		appHealsy.clients.remove(key);
	}

	public static HashMap<String, Client> getClients() {
		return appHealsy.clients;
	}
}
