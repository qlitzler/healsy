package qlitzler.com.healsy.device;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;

import qlitzler.com.healsy.ActivityHome;
import qlitzler.com.healsy.AppHealsy;
import qlitzler.com.healsy.BackgroundServiceIO;
import qlitzler.com.healsy.R;
import qlitzler.com.healsy.rfcomm.Client;
import qlitzler.com.lasagna.interfaces.IVAdapterCallback;
import qlitzler.com.lasagna.interfaces.IVLifecycle;
import qlitzler.com.lasagna.list.IVVHList;

/**
 * Created by qlitzler on 01/05/16.
 */
public class VHDevice extends IVVHList<BluetoothDevice> {

	private TextView name;
	private TextView address;
	private Switch switcher;
	private ActivityHome activity;
	private Intent service;

	public VHDevice(IVLifecycle lifecycle, View view, int typeView, IVAdapterCallback callback) {
		super(lifecycle, view, typeView, callback);
		activity = ((ActivityHome) ((FragmentListDevice) lifecycle).getActivity());
		service = new Intent(activity, BackgroundServiceIO.class);
	}

	@Override
	public void enumerate(View view, int typeView) {
		name = (TextView) view.findViewById(R.id.device_name);
		address = (TextView) view.findViewById(R.id.device_address);
		switcher = (Switch) view.findViewById(R.id.device_switch);
	}

	@Override
	public void update(BluetoothDevice device, int typeView) {
		name.setText(device.getName());
		address.setText(device.getAddress());
		lifecycle.subscribe(RxView.clicks(switcher).subscribe(v -> switching(device)));
	}

	private void switching(BluetoothDevice device) {
		if (switcher.isChecked()) {
			Client client = new Client(device, AppHealsy.HEALSY_UUID);
			AppHealsy.addClient(device.getName(), client);
			service.setData(Uri.parse(BackgroundServiceIO.CLIENT_CREATE));
		} else {
			service.setData(Uri.parse(BackgroundServiceIO.CLIENT_CLOSE));
		}
		service.putExtra(BackgroundServiceIO.DEVICE_NAME, device.getName());
		activity.startService(service);
	}

	public void setChecked(boolean state) {
		switcher.setChecked(state);
	}

	public boolean getChecked() { return switcher.isChecked(); }
}
