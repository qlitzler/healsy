package qlitzler.com.healsy.device;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewStub;

import com.jakewharton.rxbinding.support.v4.widget.RxSwipeRefreshLayout;

import java.util.ArrayList;

import qlitzler.com.healsy.ActivityHome;
import qlitzler.com.healsy.AppHealsy;
import qlitzler.com.healsy.BackgroundServiceIO;
import qlitzler.com.healsy.R;
import qlitzler.com.healsy.messenger.ActivityMessenger;
import qlitzler.com.lasagna.interfaces.IVAdapterCallback;
import qlitzler.com.lasagna.interfaces.IVLifecycle;
import qlitzler.com.lasagna.list.IVAdapterList;
import qlitzler.com.lasagna.list.IVFragmentList;
import rx.Observable;

/**
 * Created by qlitzler on 01/05/16.
 */
public class FragmentListDevice extends IVFragmentList<BluetoothDevice> {

	public static final String TAG = FragmentListDevice.class.getName();

	private SwipeRefreshLayout swipeRefreshLayout;
	private ViewStub errorNoDevice;

	@Override
	public void enumerate(View view) {
		super.enumerate(view);
		swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh_device);
		errorNoDevice = (ViewStub) view.findViewById(R.id.error_no_device);
	}

	@Override
	public void instantiate(View view) {
		super.instantiate(view);
		adapterList.setList(new ArrayList<>());
		subscribe(
			RxSwipeRefreshLayout
				.refreshes(swipeRefreshLayout)
				.subscribe(s -> ((ActivityHome) activity).discoverDevices())
		);
	}

	@Override
	public void update(Context context) {
		super.update(context);
		((ActivityHome) activity).discoverDevices();
	}

	public void addDevice(BluetoothDevice device) {
		if (adapterList.getList() == null) {
			addItem(device);
		} else if (device.getName() != null && device.getAddress() != null) {
			BluetoothDevice find = Observable.from(adapterList.getList())
				.filter(item -> item.getAddress().equals(device.getAddress()))
				.toBlocking()
				.firstOrDefault(null);
			if (find == null) {
				addItem(device);
			}
		}
	}

	public void addItem(BluetoothDevice device) {
		adapterList.addObject(device);
		adapterList.notifyDataSetChanged();
		errorNoDevice.setVisibility(View.GONE);
	}

	public void discoveryStarted() {
		swipeRefreshLayout.setRefreshing(true);
		adapterList.getList().clear();
		adapterList.notifyDataSetChanged();
	}

	public void discoveryFinished() {
		swipeRefreshLayout.setRefreshing(false);
		if (adapterList.getItemCount() == 0) {
			if (errorNoDevice.getParent() != null) {
				errorNoDevice.inflate();
			} else {
				errorNoDevice.setVisibility(View.VISIBLE);
			}
		}
	}

	public void setChecked(String name, boolean checked) {
		Observable
			.from(adapterList.getList())
			.zipWith(Observable.range(0, adapterList.getItemCount()), Pair::new)
			.filter(device -> device.first.getName().equals(name.trim()))
			.subscribe(
				pair -> {
					((VHDevice) recyclerView
						.getChildViewHolder(recyclerView.getChildAt(pair.second)))
						.setChecked(checked);
				}
			);
	}

	@Override
	public void adapterCallback(int position) {
		VHDevice vhDevice = (VHDevice) recyclerView.getChildViewHolder(recyclerView.getChildAt(position));
		if (vhDevice.getChecked()) {
			String type = BackgroundServiceIO.CLIENT_MESSAGE;
			String client = Build.MODEL;
			String name = adapterList.getObject(position).getName();

			Intent intent = new Intent(activity, ActivityMessenger.class);
			if (AppHealsy.getClient(name) != null) {
				type = BackgroundServiceIO.SERVER_MESSAGE;
				client = name;
			}
			intent.putExtra(ActivityMessenger.TYPE, type);
			intent.putExtra(ActivityMessenger.CLIENT, client);
			intent.putExtra(ActivityMessenger.NAME, name);
			startActivity(intent);
		}
	}

	@Override
	public String getIVTag() {
		return TAG;
	}

	@Override
	protected int getIdLayout() {
		return R.layout.fragment_list_device;
	}

	@Override
	protected int getIdList() {
		return R.id.list_device;
	}

	@Override
	protected IVAdapterList<BluetoothDevice> getAdapterList(IVLifecycle lifecycle, LayoutInflater inflater, IVAdapterCallback callback) {
		return new AdapterListDevice(lifecycle, inflater, callback);
	}
}
