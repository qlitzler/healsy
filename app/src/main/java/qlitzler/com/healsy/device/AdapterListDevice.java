package qlitzler.com.healsy.device;

import android.bluetooth.BluetoothDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import qlitzler.com.healsy.R;
import qlitzler.com.lasagna.interfaces.IVAdapterCallback;
import qlitzler.com.lasagna.interfaces.IVLifecycle;
import qlitzler.com.lasagna.list.IVAdapterList;
import qlitzler.com.lasagna.list.IVVHList;

/**
 * Created by qlitzler on 01/05/16.
 */
public class AdapterListDevice extends IVAdapterList<BluetoothDevice> {

	public AdapterListDevice(IVLifecycle lifecycle, LayoutInflater inflater,
							 IVAdapterCallback callback
	) {
		super(lifecycle, inflater, callback);
	}

	@Override
	protected View inflateView(LayoutInflater inflater, ViewGroup parent, int typeView) {
		return inflater.inflate(R.layout.single_device_item, parent, false);
	}

	@Override
	protected IVVHList<BluetoothDevice> getViewHolder(IVLifecycle lifecycle, View view, int typeView,
													  IVAdapterCallback callback
	) {
		return new VHDevice(lifecycle, view, typeView, callback);
	}
}
