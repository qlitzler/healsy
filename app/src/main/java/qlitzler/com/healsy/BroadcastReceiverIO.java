package qlitzler.com.healsy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.lang.ref.WeakReference;

import qlitzler.com.healsy.messenger.ActivityMessenger;
import qlitzler.com.healsy.messenger.AdapterListMessenger;
import qlitzler.com.healsy.rfcomm.Protocol;

/**
 * Created by qlitzler on 08/05/16.
 */
public class BroadcastReceiverIO extends BroadcastReceiver {

	public static final String CLIENT_CREATE = "client_create";
	public static final String CLIENT_CLOSE = "client_close";
	public static final String MESSAGE = "message";
	public static final String ERROR = "error";

	private WeakReference<ActivityHome> home;
	private WeakReference<ActivityMessenger> messenger;

	public BroadcastReceiverIO(ActivityHome activity) {
		home = new WeakReference<>(activity);
	}

	public BroadcastReceiverIO(ActivityMessenger activity) {
		messenger = new WeakReference<>(activity);
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();

		switch (action) {
			case CLIENT_CREATE:
				home.get()
					.findFragmentListDevice()
					.setChecked(intent.getStringExtra(Protocol.CLIENT_CREATE), true);
				break;
			case CLIENT_CLOSE:
				home.get()
					.findFragmentListDevice()
					.setChecked(intent.getStringExtra(Protocol.CLIENT_CLOSE), false);
				break;
			case MESSAGE:
				messenger.get()
					.findFragmentListMessenger()
					.addMessage(AdapterListMessenger.RECEIVER, intent.getStringExtra(Protocol.MESSAGE));
				break;
			case ERROR:
				if (home != null) {
					home.get().displayError(intent.getStringExtra(Protocol.ERROR));
				}
				if (messenger != null) {
					messenger.get().displayError(intent.getStringExtra(Protocol.ERROR));
				}
				break;
		}
	}

	public static IntentFilter filterHome() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(BroadcastReceiverIO.CLIENT_CLOSE);
		filter.addAction(BroadcastReceiverIO.CLIENT_CREATE);
		filter.addAction(BroadcastReceiverIO.ERROR);
		return filter;
	}

	public static IntentFilter filterMessenger() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(BroadcastReceiverIO.MESSAGE);
		filter.addAction(BroadcastReceiverIO.ERROR);
		return filter;
	}
}
