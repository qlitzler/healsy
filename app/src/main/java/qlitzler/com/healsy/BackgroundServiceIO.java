package qlitzler.com.healsy;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.util.Pair;

import qlitzler.com.healsy.rfcomm.Client;
import qlitzler.com.healsy.rfcomm.Protocol;
import qlitzler.com.healsy.rfcomm.Server;

/**
 * Created by qlitzler on 08/05/16.
 */
public class BackgroundServiceIO extends IntentService {

	public static final String PREFIX = "qlitzler.com.healsy.";
	public static final String SERVER_CREATE = PREFIX + "server_create";
	public static final String SERVER_MESSAGE = PREFIX + "server_message";
	public static final String CLIENT_CREATE = PREFIX + "client_create";
	public static final String CLIENT_CLOSE = PREFIX + "client_close";
	public static final String CLIENT_MESSAGE = PREFIX + "client_message";

	public static final String MESSAGE = "message";
	public static final String DEVICE_NAME = "device_name";

	private Server server;

	public BackgroundServiceIO() {
		super("BackgroundServiceIO");
	}

	public BackgroundServiceIO(String name) {
		super(name);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		server = AppHealsy.getServer();
	}

	private void readServer(Pair<Integer, String> pair) {
		Log.e(AppHealsy.LOG_SERVER, pair.second);
		String message = pair.second;
		if (message.startsWith(Protocol.CLIENT_CREATE)) {
			broadcastClientCreate(message);
		} else if (message.startsWith(Protocol.CLIENT_CLOSE)) {
			broadcastClientClose(message);
		} else if (message.startsWith(Protocol.MESSAGE)) {
			broadcastMessage(message);
		}
	}

	private void broadcastClientCreate(String message) {
		Intent intent = new Intent(BroadcastReceiverIO.CLIENT_CREATE);
		intent.putExtra(Protocol.CLIENT_CREATE, message.substring(Protocol.CLIENT_CREATE.length()));
		sendBroadcast(intent);
	}

	private void broadcastClientClose(String message) {
		Intent intent = new Intent(BroadcastReceiverIO.CLIENT_CLOSE);
		intent.putExtra(Protocol.CLIENT_CLOSE, message.substring(Protocol.CLIENT_CLOSE.length()));
		sendBroadcast(intent);
	}

	private void broadcastMessage(String message) {
		Intent intent = new Intent(BroadcastReceiverIO.MESSAGE);
		intent.putExtra(Protocol.MESSAGE, message.substring(Protocol.MESSAGE.length()));
		sendBroadcast(intent);
	}

	private void broadcastError(String message) {
		Intent intent = new Intent(BroadcastReceiverIO.ERROR);
		intent.putExtra(Protocol.ERROR, message);
		sendBroadcast(intent);
	}

	private void readClient(Pair<Integer, String> pair) {
		Log.e(AppHealsy.LOG_CLIENT, pair.second);
		String message = pair.second;
		if (message.startsWith(Protocol.MESSAGE)) {
			broadcastMessage(message);
		}
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String data = intent.getDataString();
		String name = intent.getStringExtra(DEVICE_NAME);
		Client client = AppHealsy.getClient(name);

		switch (data) {
			case SERVER_CREATE:
				server
					.start(
						triplet -> server.read().subscribe(this::readServer, throwable1 -> {
							Log.e(AppHealsy.LOG_SERVER, "Read error");
						}),
						throwable -> {
							Log.e(AppHealsy.LOG_SERVER, "Creation error");
							broadcastError("Error creating server.");
						}
					);
				break;
			case CLIENT_CREATE:
				client.start(
					triplet -> {
						client.write(
							Protocol.newClient(AppHealsy.bluetoothAdapter.getName()),
							throwable -> broadcastError("Could not open channel with client.")
						);
						client.read().subscribe(
							this::readClient,
							throwable -> broadcastClientClose(Protocol.closeClient(name))
						);
					},
					throwable -> {
						Log.e(AppHealsy.LOG_CLIENT, "Creation Error");
						broadcastClientClose(Protocol.newClient(name));
						broadcastError("Error creating client.");
					}
				);
				return;
			case CLIENT_CLOSE:
				if (client != null && client.isConnected()) {
					client.write(
						Protocol.closeClient(AppHealsy.bluetoothAdapter.getName()),
						throwable -> {}
					);
					client.shutdown(throwable -> {});
					AppHealsy.removeClient(name);
				} else {
					server.closeIO(throwable -> {});
				}
				break;
			case CLIENT_MESSAGE:
				server.write(
					intent.getStringExtra(MESSAGE),
					throwable -> broadcastError("Could not write to client.")
				);
				break;
			case SERVER_MESSAGE:
				client.write(
					intent.getStringExtra(MESSAGE),
					throwable -> broadcastError("Could not write to server.")
				);
				break;
		}
	}
}
